//
//  YoViewController.swift
//  FirebaseDemo
//
//  Created by Michael on 2018/12/25.
//  Copyright © 2018 Zencher. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class YoViewController: UIViewController {

    @IBOutlet weak var dataLabel: UILabel!
    @IBOutlet weak var yoMessage: UITextField!
    
    lazy var ref = Database.database().reference()
    override func viewDidLoad() {
        super.viewDidLoad()
        var refHandle = ref.observe(DataEventType.value, with: { (snapshot) in
            let postDict = snapshot.value as? [String : AnyObject] ?? [:]
            self.dataLabel.text = postDict["Label"] as? String
        })

    }
    
    @IBAction func send(_ sender: Any) {
        ref.setValue(["Label": yoMessage.text])

    }
    
    @IBAction func logout(_ sender: Any) {
        if Auth.auth().currentUser != nil {
            do {
                try Auth.auth().signOut()
                dismiss(animated: true, completion: nil)
                
                
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }

        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
