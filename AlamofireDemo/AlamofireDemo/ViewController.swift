//
//  ViewController.swift
//  AlamofireDemo
//
//  Created by Michael on 2018/12/17.
//  Copyright © 2018 Zencher. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = "https://www.apple.com"
        
        Alamofire.request(url).response { (response :DefaultDataResponse) in
            let html = String(data:response.data!, encoding: .utf8)
            print(html)
        }
    }
    
    func requestWith(endUrl: String, imageData: Data?, parameters: [String : Any] ){
        let url = endUrl
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            if let data = imageData{
                multipartFormData.append(data, withName: "image", fileName: "image.png", mimeType: "image/png")
            }
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                    if let err = response.result.error {
                        print("Error in upload:", err)
                        return
                    }
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
            }
        }
    }

    @IBAction func upload(_ sender: Any) {
        let image = UIImage(named:"animal.jpg")
        requestWith(endUrl: "http://localhost:8080/upload", imageData:image!.pngData(), parameters: [:])
    }
    
    
    @IBAction func openData(_ sender: Any) {
        let jsonURL = "https://data.taipei/opendata/datalist/apiAccess?scope=resourceAquire&rid=24c9f8fe-88db-4a6e-895c-498fbc94df94"
        
        Alamofire.request(jsonURL).responseJSON { (response) in
            do {
                let decoder = JSONDecoder()
                let openData = try decoder.decode(School.self, from: response.data!)
                print(openData.result.results[0].o_tlc_agency_name)
            } catch {
                print("\(error)")
            }
        }
    }
}

