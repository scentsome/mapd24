//
//  ViewController.swift
//  DelegatePractice
//
//  Created by Michael on 2018/12/17.
//  Copyright © 2018 Zencher. All rights reserved.
//

import UIKit

class ViewController: UIViewController,InputViewControllerDelegate {
    func didSend(text: String) {
        myLabel.text = text
        dismiss(animated: true, completion: nil)
        
        // present
    }
    

    @IBOutlet weak var myLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let inputVC:InputViewController =  segue.destination as! InputViewController
        inputVC.delegate = self
        
    }
}

