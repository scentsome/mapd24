//
//  ViewController.swift
//  TableViewDemo
//
//  Created by Michael on 2018/12/17.
//  Copyright © 2018 Zencher. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var searchController = UISearchController(searchResultsController: nil)

    var tasks:[CellData] = [CellData(title:"t1",isSelect:false),CellData(title:"t2",isSelect:false),CellData(title:"t3",isSelect:false),CellData(title:"t4",isSelect:false),CellData(title:"t5",isSelect:false),CellData(title:"t1",isSelect:false),CellData(title:"t2",isSelect:false),CellData(title:"t3",isSelect:false),CellData(title:"t4",isSelect:false),CellData(title:"t5",isSelect:false)
        ,CellData(title:"t1",isSelect:false),CellData(title:"t2",isSelect:false),CellData(title:"t3",isSelect:false),CellData(title:"t4",isSelect:false),CellData(title:"t5",isSelect:false),CellData(title:"t1",isSelect:false),CellData(title:"t2",isSelect:false),CellData(title:"t3",isSelect:false),CellData(title:"t4",isSelect:false),CellData(title:"t5",isSelect:false)
    ]
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchController.searchBar.scopeButtonTitles = ["Prefix", "Subfix"]
        self.tableView.tableHeaderView = searchController.searchBar
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    
    
    
    
    

}

extension ViewController: UITableViewDataSource {
    
    
    fileprivate func configureCell(_ cellData: CellData, _ cell: UITableViewCell?) {
        if cellData.isSelect {
            cell?.accessoryType = .checkmark
        } else {
            cell?.accessoryType = .none
        }
        cell?.textLabel?.text = cellData.title
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        var cell:UITableViewCell? = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        
        var cell:UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "Cell")

        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        }
        
        let cellData = tasks[indexPath.row]
        
        
        
        configureCell(cellData, cell)
        
        return cell!
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
}

extension ViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell:UITableViewCell? = tableView.cellForRow(at: indexPath)
        
        tasks[indexPath.row].isSelect = !tasks[indexPath.row].isSelect
        configureCell(tasks[indexPath.row], cell)
    }
}

