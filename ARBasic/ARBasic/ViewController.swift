//
//  ViewController.swift
//  ARBasic
//
//  Created by Michael on 2018/12/27.
//  Copyright © 2018 Zencher. All rights reserved.
//

import UIKit
import ARKit
import SceneKit

class ViewController: UIViewController {
    
    var focusNode: SCNNode!
    var focusPoint:CGPoint!

    @IBOutlet weak var arscnView: ARSCNView!
    
    
    func addFocusNode() {
        
        var geometry:SCNGeometry = SCNGeometry()
        geometry = SCNBox(width: 0.1, height: 0.1, length: 0.1, chamferRadius: 0)
        let material = SCNMaterial()
        material.diffuse.contents = UIColor.yellow
        material.lightingModel = .physicallyBased
        geometry.materials = [material]
        focusNode = SCNNode(geometry: geometry)
        let position:SCNVector3 = SCNVector3(x: 0.0, y: 0.0, z: -0.05)
        focusNode.position = position
        
        arscnView.scene.rootNode.addChildNode(focusNode)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        arscnView.delegate = self
        
        arscnView.showsStatistics = true
        
        let scene = SCNScene()
        
        arscnView.scene = scene
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(ViewController.orientationChanged),
                                               name: UIDevice.orientationDidChangeNotification,
                                               object: nil)
        
        addFocusNode()
        focusPoint = CGPoint(x: view.center.x,
                             y: view.center.y + view.center.y * 0.25)
    }
    @objc func orientationChanged() {
        
        focusPoint = CGPoint(x: view.center.x,
                             y: view.center.y + view.center.y * 0.25)
        
    }
    
    func updateFocusNode() {
        
        let results = self.arscnView.hitTest(self.focusPoint,
                                             types: [.existingPlaneUsingExtent])
        if results.count == 1 {
            if let match = results.first {
                let t = match.worldTransform
                self.focusNode.position = SCNVector3( x: t.columns.3.x,
                                                      y: t.columns.3.y,
                                                      z: t.columns.3.z)
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let configuration = ARWorldTrackingConfiguration()
        configuration.worldAlignment = .gravity
        configuration.planeDetection = .horizontal
        arscnView.session.run(configuration)
        
        arscnView.debugOptions = [.showFeaturePoints, .showWorldOrigin]
        
        addBox()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        arscnView.session.pause()
    }

    func addBox() {
        
        var geometry:SCNGeometry = SCNGeometry()
        geometry = SCNBox(width: 0.01, height: 0.01,
                          length: 0.01, chamferRadius: 0)
        let geometerNode:SCNNode = SCNNode(geometry: geometry)
        let position:SCNVector3 = SCNVector3(x: 0.0, y: 0.0, z: -0.05)
        geometerNode.position = position
        
        arscnView.scene.rootNode.addChildNode(geometerNode)
    }

}

extension ViewController : ARSCNViewDelegate {
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor){
        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
        
        DispatchQueue.main.async {
            
            let planeNode = self.createARPlaneNode(planeAnchor: planeAnchor)
            
            node.addChildNode(planeNode)
        }

    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor){
        func renderer(_ renderer: SCNSceneRenderer,
                      didUpdate node: SCNNode, for anchor: ARAnchor) {
            
            guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
            
            DispatchQueue.main.async {
                
                self.updateARPlaneNode(planeNode: node.childNodes[0],
                                       planeAchor: planeAnchor)
            }
        }
    }
    
    func updateARPlaneNode(planeNode: SCNNode, planeAchor: ARPlaneAnchor) {
        
        let planeGeometry = planeNode.geometry as! SCNPlane
        planeGeometry.width = CGFloat(planeAchor.extent.x)
        planeGeometry.height = CGFloat(planeAchor.extent.z)
        
        planeNode.position = SCNVector3Make(planeAchor.center.x, 0,
                                            planeAchor.center.z)
    }
    
    func createARPlaneNode(planeAnchor: ARPlaneAnchor) -> SCNNode {
        
        let planeGeometry = SCNPlane(width: CGFloat(planeAnchor.extent.x),
                                     height: CGFloat(planeAnchor.extent.z))
        let material = SCNMaterial()
        material.diffuse.contents = UIColor.blue
        material.lightingModel = .physicallyBased
        planeGeometry.materials = [material]
        
        let planeNode = SCNNode(geometry: planeGeometry)
        planeNode.position = SCNVector3Make(
            planeAnchor.center.x, 0, planeAnchor.center.z)
        planeNode.transform = SCNMatrix4MakeRotation(-Float.pi / 2, 1, 0, 0)
        
        return planeNode
    }
    
    func renderer(_ renderer: SCNSceneRenderer,
                  updateAtTime time: TimeInterval) {
        DispatchQueue.main.async {
            
            self.updateFocusNode()
        }
    }
}
