import FluentSQLite
import Vapor
import FluentPostgreSQL
import Leaf

/// Called before your application initializes.
public func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {
    /// Register providers first
    
    
    
    try services.register(FluentSQLiteProvider())

    
    try services.register(FluentPostgreSQLProvider())
    config.prefer(DictionaryKeyedCache.self, for: KeyedCache.self)

    /// Register routes to the router
    let router = EngineRouter.default()
    try routes(router)
    services.register(router, as: Router.self)

    /// Register middleware
    var middlewares = MiddlewareConfig() // Create _empty_ middleware config
    middlewares.use(FileMiddleware.self) // Serves files from `Public/` directory
    middlewares.use(ErrorMiddleware.self) // Catches errors and converts to HTTP response
    services.register(middlewares)
    
    
//    var databases = DatabasesConfig()
    let databaseConfig = PostgreSQLDatabaseConfig(
        hostname: "localhost",
        username: "hello",
        database: "hello",
        password: "password")
    let postgreDB = PostgreSQLDatabase(config: databaseConfig)
    


    // Configure a SQLite database
    let sqlite = try SQLiteDatabase(storage: .memory)

    /// Register the configured SQLite database to the database config.
    var databases = DatabasesConfig()
    
    databases.add(database: sqlite, as: .sqlite)
    
    databases.add(database: postgreDB, as: .psql)
    services.register(databases)

    /// Configure migrations
    var migrations = MigrationConfig()
    migrations.add(model: Todo.self, database: .sqlite)
    migrations.add(model: Person.self, database: .psql)
    services.register(migrations)

    let myServerConfig = NIOServerConfig.default(hostname: "localhost", port: 8080)
    services.register(myServerConfig)
    let leafProvider = LeafProvider()
    try services.register(leafProvider)
    config.prefer(LeafRenderer.self, for: ViewRenderer.self)
}
