//
//  Person.swift
//  App
//
//  Created by Michael on 2018/12/21.
//

import Vapor
import FluentPostgreSQL

final class Person: Content {
    var id:UUID?
    var name: String
    var age: Int
    
    init(name: String, age: Int) {
        self.name = name
        self.age = age
    }
}

extension Person: PostgreSQLUUIDModel {}
extension Person: Migration {}
extension Person: Parameter {}

