//
//  ViewController.swift
//  DelegateClsoure
//
//  Created by Michael on 2018/12/17.
//  Copyright © 2018 Zencher. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var myLabel: UILabel!
    
    func updateUI(data:String) {
        self.myLabel.text = data
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var inputVC = segue.destination as! InputViewController
        inputVC.callBack = {(data:String) in
            self.myLabel.text = data
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


}

