//
//  InputViewController.swift
//  DelegateClsoure
//
//  Created by Michael on 2018/12/17.
//  Copyright © 2018 Zencher. All rights reserved.
//

import UIKit

class InputViewController: UIViewController {

    @IBOutlet weak var inputField: UITextField!
    
    var callBack:((String) -> Void)?
    
    
    @IBAction func done(_ sender: Any) {
        callBack?(inputField.text!)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
