//
//  BarViewController.swift
//  ChartsDemo
//
//  Created by Michael on 2018/12/27.
//  Copyright © 2018 Zencher. All rights reserved.
//

import UIKit
import Charts

class BarViewController: UIViewController {
    @IBOutlet weak var barChartView: BarChartView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupData()
        // Do any additional setup after loading the view.
    }
    func setupData(){
        let values:[Double] = [20.0, 4.0, 6.0, 3.0, 12.0, 16.0, 4.0, 18.0, 2.0, 4.0, 5.0, 4.0]
        var dataEntries:[BarChartDataEntry] = []
        
        for (index,value) in values.enumerated() {
            let entry = BarChartDataEntry(x: Double(index), y:  Double(value))
            dataEntries.append(entry)
        }
        let dataSet = BarChartDataSet(values: dataEntries, label: "Units Sold")
        let barChartData = BarChartData(dataSets: [dataSet])
        self.barChartView.data = barChartData
        self.barChartView.xAxis.labelPosition = .bottom
        
        
        let limitLine = ChartLimitLine(limit: 11.0,
                                       label: "Target")
        self.barChartView.rightAxis.addLimitLine(limitLine)


    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
