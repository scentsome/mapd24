//
//  ViewController.swift
//  ChartsDemo
//
//  Created by Michael on 2018/12/27.
//  Copyright © 2018 Zencher. All rights reserved.
//

import UIKit
import Charts

class ViewController: UIViewController {

    @IBOutlet weak var pieChartView: PieChartView!
    override func viewDidLoad() {
        super.viewDidLoad()
        pieChartView.noDataText = "請準備資料"
        pieChartView.delegate = self
        prepareData()
    }
    func prepareData() {
//        let pieChartRawData:NSDictionary = ["iOS 8":84, "iOS 7":14, "Earlier":2]
        var yValues:[PieChartDataEntry] = []
        
//        pieChartRawData.enumerateKeysAndObjects { (key, obj, stop) in
//            let entry:PieChartDataEntry = PieChartDataEntry(
//                value: obj as! Double,
//                label: key as? String)
//            yValues.append(entry)
//        }
        
        let pieChartDict = ["iOS 8":84, "iOS 7":14, "Earlier":2]
        
        for (label, value) in pieChartDict {
            yValues.append(PieChartDataEntry(value: Double(value), label: label))
        }
        
        
        let dataSet:PieChartDataSet = PieChartDataSet(values: yValues,
                                                      label: "iOS Distribution")
        
        var colors:[NSUIColor] = []
        colors.append(contentsOf: ChartColorTemplates.pastel())
        dataSet.colors = colors
        let pieChartData:PieChartData = PieChartData(dataSet: dataSet)
        pieChartView.data = pieChartData
        
        var pFormatter:NumberFormatter = NumberFormatter()
        
        pFormatter.numberStyle = .percent
        pFormatter.maximumFractionDigits = 1
        pFormatter.multiplier = 1
        pFormatter.percentSymbol = "%"
    pieChartData.setValueFormatter(DefaultValueFormatter(formatter:pFormatter))
        pieChartData.setValueFont(UIFont(name: "Helvetica", size: 22))
        pieChartData.setValueTextColor(UIColor.white)
    }

    @IBAction func highlight(_ sender: Any) {
        pieChartView.highlightValue(x: 2, y: 0, dataSetIndex: 0)
        pieChartView.rotationAngle = 90
    }
    
}

extension ViewController : ChartViewDelegate {
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        print("Did select \(entry)")
        let pieDataEntry = entry as! PieChartDataEntry
        chartView.chartDescription?.text = pieDataEntry.label
    }
    
    func chartValueNothingSelected(_ chartView: ChartViewBase) {
        print("Nothing selected")
    }

}

