//
//  ViewController.swift
//  SceneDemo
//
//  Created by Michael on 2018/12/27.
//  Copyright © 2018 Zencher. All rights reserved.
//

import UIKit
import SceneKit

class ViewController: UIViewController {

    @IBOutlet weak var scnView: SCNView!
    var scnScene:SCNScene? = nil
    var cameraNode:SCNNode? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupScene()
        setupCamera()
        addBox()
    }
    func setupScene() {
        scnScene = SCNScene()
        scnView?.scene = scnScene
    }
    func setupCamera() {
        cameraNode = SCNNode()
        cameraNode?.camera = SCNCamera()
        cameraNode?.position = SCNVector3Make(0.0, 0.0, 10.0)
        scnScene?.rootNode.addChildNode(cameraNode!)
    }
    func setupView() {
        scnView?.showsStatistics = true
        scnView?.allowsCameraControl = true
        scnView?.autoenablesDefaultLighting = true
    }
    func addBox() {
        
        var geometry:SCNGeometry = SCNGeometry()
        geometry = SCNBox(width: 1.0, height: 1.0, length: 1.0, chamferRadius: 0)
        
        let geometerNode:SCNNode = SCNNode(geometry: geometry)
        
        geometerNode.position = SCNVector3(0, 0, 0)
        scnScene?.rootNode.addChildNode(geometerNode)
        geometerNode.physicsBody = SCNPhysicsBody(type: .dynamic, shape: nil)
        
        geometerNode.physicsBody = SCNPhysicsBody(type: .dynamic, shape: nil)
        let force = SCNVector3(x: 0, y: 10 , z: 0)
        let position = SCNVector3(x: 0, y: 0, z: 0)
        geometerNode.physicsBody?.applyForce(force, at: position, asImpulse: true)

    }


    override var shouldAutorotate: Bool {
        return true
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}

